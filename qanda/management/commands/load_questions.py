from django.core.management.base import BaseCommand

from qanda import elasticsearch
from qanda.models import Question


class Command(BaseCommand):
    help = "Load all questions."

    def handle(self, *args, **options):
        queryset = Question.objects.all()
        all_loaded = elasticsearch.bulk_load(queryset)

        if all_loaded:
            self.stdout.write(self.style.SUCCESS('Successfully loaded all questions into ES'))
        else:
            self.stdout.write(self.style.WARNING('Some error. See logs'))
